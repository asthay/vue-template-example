import Vue from 'vue';
import Vuex, { StoreOptions } from 'vuex';
import {RootState} from './interfaces/types';
Vue.use(Vuex);

const store: StoreOptions<RootState> = {
  state: {
    version: '1.0.0',
  },
  modules: {
    // lodader
  }
}

export default new Vuex.Store<RootState>(store);