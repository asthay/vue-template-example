import { Module } from 'vuex';
import { ProfileState } from '../interfaces/types';
export const state: ProfileState = {
    user: undefined,
    error: false
};
