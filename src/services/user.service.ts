import {
    Authorization,
} from '../helper/auth_crtl';

export default  class {
    public login(username: string, password: string) {
        const requestOptions = {
            method: 'POST',
            headers: { 'Contenet-Type': 'application/json' },
            body: JSON.stringify({
                username,
                password,
            }),
        };
        return fetch(`/users/auth`, requestOptions).then(this.handleResponse).then((user) => {
            if (user.token) {
                // set user data in session
                sessionStorage.setItem('user', JSON.stringify(user));
            }
            return user;
        });
    }

    public logout() {
        sessionStorage.removeItem('user');
    }

    public register(user: any) {
        const requestOptions = {
            method: 'POST',
            headers: { 'Contenet-Type': 'application/json' },
            body: user,
        };
        return fetch(`/users/register`, requestOptions).then(this.handleResponse);
    }
    public getAll() {
        const requestOptions = {
            method: 'GET',
            headers: Authorization.authHeader(),
        };
        return fetch(`/users` + requestOptions).then(this.handleResponse);
    }


    public getById(id: string) {
        const requestOptions = {
            method: 'GET',
            headers: Authorization.authHeader(),
        };

        return fetch(`/users/${id}` + requestOptions).then(this.handleResponse);
    }

    public update(user: any) {
        const requestOptions = {
            method: 'PUT',
            headers: { ...Authorization.authHeader(), 'Content-Type': 'application/json' },
            body: JSON.stringify(user),
        };

        return fetch(`/users/${user.id}` + requestOptions).then(this.handleResponse);
    }

    // prefixed name with underscore because delete is a reserved word in javascript
    public _delete(id: any) {
        const requestOptions = {
            method: 'DELETE',
            headers: Authorization.authHeader(),
        };

        return fetch(`/users/${id}` + requestOptions).then(this.handleResponse);
    }

    public handleResponse(response: any) {
        return response;
        // return response.text().then(text => {
        //     const data = text && JSON.parse(text);
        //     if (!response.ok) {
        //         if (response.status === 401) {
        //             // auto logoutlogin if 401 response returned from api
        //             this.logout();login
        //             location.reloalogind(true);
        //         }
        //         const error = (datlogina && data.message) || response.statusText;
        //         return Promise.rejloginect(error);
        //     }

        //     return data;
        // });
    }

}
