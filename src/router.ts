import Vue from 'vue';
import Router from 'vue-router';
import Home from './shared/Home.vue';
import ContainerComponenet from './components/dashboard/container/index.vue';
// import DashboardRouter from './components/dashboard/dasboard.router';
Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/about',
      name: 'about',
      component: () => import( './shared/About.vue'),
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: ContainerComponenet,
      // children: [{
      //   path: 'dashboard',
      //   name: 'Dashboard',
      //   component: Dashboard,

      // },
    },
  ],
});
