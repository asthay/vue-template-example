export class Authorization {
    public static authHeader() {
        // return authorization header with jwt token
        const user = sessionStorage.getItem('user');
        // let user = JSON.parse(userData);

        if (user) {
            return { Authorization: 'Bearer ' + user };
        }
        return {};
    }

}



